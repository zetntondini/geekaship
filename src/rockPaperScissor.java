/**
 * Created by intern on 2015/10/09.
 */
import java.awt.print.Paper;
import java.util.Random;
import java.util.Scanner;

public class rockPaperScissor {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        Random AI = new Random();
        int rock = 0;
        int paper = 0;
        int scissor = 0;
        int computerGame;
        String computer, Selector;

        for (int i = 0; i < 2; i++) {

            System.out.print("rock, paper or scissors: ");
            Selector = scan.nextLine();

            computerGame = AI.nextInt(3);

            if (computerGame == 0) {
                if (Selector .equals("rock")) {
                    rock++;
                    System.out.println("win\n");

                }
                if (Selector.equals("paper")) {
                    paper++;
                    System.out.println("win\n");
                }

                if (Selector.equals("scissors")) {
                    scissor++;
                    System.out.println("lose\n");

                }
            } else if (computerGame == 1) {
                if (Selector.equals("rock")) {
                    rock++;
                    System.out.println("lose \n");

                }
                if (Selector.equals("paper")) {
                    paper++;
                    System.out.println("lose\n");
                }

                if (Selector.equals("scissors")) {
                    scissor++;
                    System.out.println("win\n");

                }
                if (computerGame == 2) {
                    if (Selector.equals("rock")) {
                        rock++;
                        System.out.println("lose \n");

                    }
                    if (Selector.equals("paper")) {
                        paper++;
                        System.out.println("win\n");
                    }

                    if (Selector.equals("scissors")) {
                        scissor++;
                        System.out.println("fail\n");
                    }
                }

            }
        }

    }
}