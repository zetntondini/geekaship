/**
 * Created by intern on 2015/10/06.
 */
import java.util.Scanner;

public class Rnumeral {

    public static void main(String[] args) {

        Scanner In = new Scanner(System.in);

        System.out.print("Insert a Roman Number:> ");

        char[] roman = In.nextLine().toCharArray();

        int total = 0;
        for(int i = roman.length-1; i > -1; i--){
            switch(roman[i]){
                case 'I':
                    total += value(roman[i]); break;
                case 'V':
                case 'X':
                case 'L':
                case 'C':
                case 'D':
                case 'M':
                    if(i != 0 && (value(roman[i-1]) < value(roman[i]))){
                        total += value(roman[i]) - value(roman[i-1]);
                        i--;
                    }else{
                        total += value(roman[i]);
                    }
                    break;
            }
        }
        System.out.println(total);
    }

    public static int value(char c){
        switch(c){
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'L':
                return 50;
            case 'C':
                return 100;
            case 'D':
                return 500;
            case 'M':
                return 1000;
            default:
                return 0;
        }
    }
}

