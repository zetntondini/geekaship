
/**
 * Created by intern on 2015/09/30.
 */
public class ballancing_brackets {

    public static boolean checkBrackets(String str) {
        int BBrackets = 0;
        for (char ch : str.toCharArray()) {
            if (ch == '[') {
                BBrackets++;
            } else if (ch == ']') {
                BBrackets--;
            } else {
                return false;
            }
            if (BBrackets < 0) {
                return false;
            }
        }
        return BBrackets == 0;
    }


    public static String generate(int n){
        if(n % 2 == 1){
            return null;
        }
        String ans = "";
        int openBracketsLeft = n / 2;
        int unclosed = 0;
        while(ans.length() < n){
            if(Math.random() >= .5 && openBracketsLeft > 0 || unclosed == 0){
                ans += '[';
                openBracketsLeft--;
                unclosed++;
            }else{
                ans += ']';
                unclosed--;
            }
        }
        return ans;
    }

    public static void main(String[] args){
        String[] tests = {"", "[]", "][", "[][]", "][][", "[[][]]", "[]][[]"};


        for(String test: tests){
            System.out.println(test + ": " + checkBrackets(test));
        }
    }
}

