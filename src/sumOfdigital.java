/**
 * Created by intern on 2015/09/21.
 */
public class sumOfdigital {

    public static void main(String[] args) {

        String in = new String("n");
        int n;

        System.out.print("Enter a positive integer: ");
        n = 6543;

        if (n <= 0)
            System.out.println("Negative integer.");
        else {
            int sum = 0;

            while (n != 0) {

                sum += n % 10;

                n /= 10;
            }
            System.out.println("Sum of digits: " + sum);
        }
    }
}



