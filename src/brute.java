/**
 * Created by intern on 2015/10/07.
 */import java.util.*;

public class brute {

    public static void main(String args[]){
        brute src = new brute();
        Scanner sc = new Scanner(System.in);
        String text = "brute force string search ";
        System.out.print("search a word: ");
        String pattern = sc.next();

        src.setString(text, pattern);
        int position = src.search();

        if(position != -1)
            System.out.println("word: "+ pattern + " found " + position);
        else
            System.out.print("word: " + pattern + " - not found");
    }
char[] text, pattern;
int intText, intPattern;

    public void setString(String textString,String patternString){
        text = textString.toCharArray();
        pattern = patternString.toCharArray();
        intText = textString.length();
        intPattern = patternString.length();
    }
    public int search() {
        for (int i = 0; i < intText - intPattern; i++) {
            int j = 0;
            while (j < intPattern && text[i+j] == pattern[j]) {
                j++;
            }
            if (j == intPattern) return i;
        }
        return -1;
    }
}
