package src;

/**
 * Created by intern on 2015/09/22.
 */
public class Maximum_num {

    public static void main(String[] args) {

        int[] numbers = {12,78,90,104};

        int max = 0;


        for (int i = 0; i < numbers.length; i++) {

            if (numbers[i] > max) {

                max = numbers[i];

            }

        }

        System.out.println("Maximum number in array is : " + max);

    }
}
