import java.io.File;

/**
 * Created by intern on 2015/09/21.
 */
public class DeleteFile {

    public static void main(String[] args) {
        Integer dir;
    }

    public static boolean deleteDir(File dir) {
        // if (dir.isDirectory()) {
        String[] children = dir.list();
        for (int i = 0; i < children.length; i++) {
            boolean success = deleteDir(new File(dir, children[i]));
            if (!success) {
                return false;
            }
        }


        // The directory is now empty so delete it
        return dir.delete();
    }
 }
//}