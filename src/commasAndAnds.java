/**
 * Created by intern on 2015/09/30.
 */
import java.util.Scanner;
public class commasAndAnds {

    public static String quibble(String[] words) {
        String m = "(";

        for(int wIndex = 0; wIndex < words.length; wIndex++) {
            m += words[wIndex] + (wIndex == words.length-1 ? "" :
                    wIndex == words.length-2 ? " and " :
                            ", ");

        }
        m += ")";
        return m;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter your name ");
        String name = input.nextLine();

        System.out.println("Enter your surname");
        String surname = input.nextLine();

        System.out.println("Enter your gender");
        String gender  = input.nextLine();

        System.out.println(quibble(new String[]{}));
        System.out.println(quibble(new String[]{name}));
        System.out.println(quibble(new String[]{name, surname}));
        System.out.println(quibble(new String[]{name, surname, gender}));

    }

}
