/**
 * Created by intern on 2015/10/07.
 */import java.util.Scanner;

public class goldBach {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("Enter num : ");
        String number=scanner.next();
        int N = Integer.parseInt(number);

        boolean[] isprime = new boolean[N];

        for (int i = 2; i < N; i++)
            isprime[i] = true;

        for (int i = 2; i * i < N; i++) {
            if (isprime[i]) {
                for (int j = i; i * j < N; j++)
                    isprime[i*j] = false;
            }
        }

        int primes = 0;
        for (int i = 2; i < N; i++)
            if (isprime[i]) primes++;

        System.out.println("answer.");

        int[] list = new int[primes];
        int n = 0;
        for (int i = 0; i < N; i++)
            if (isprime[i]) list[n++] = i;


        int left = 0, right = primes-1;
        while (left <= right) {
            if      (list[left] + list[right] == N) break;
            else if (list[left] + list[right]  < N) left++;
            else right--;
        }
        if (list[left] + list[right] == N)
            System.out.println(N + " = " + list[left] + " + " + list[right]);
        else
            System.out.println(N + " incorrect");
    }

}


