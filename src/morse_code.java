/**
 * Created by intern on 2015/09/30.
 */
import  java.util.Scanner;

public class morse_code {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.print("enter string for translation:");
        String input = scan.nextLine();
        System.out.println( morse_code(input));

    }

    public static String  morse_code  (String s) {

        String answer = "";
        s = s.toUpperCase();

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);

            if (ch == 'A') answer += ".-";
            else if (ch == 'B') answer += "-...";
            else if (ch == 'C') answer += "-.-.";
            else if (ch == 'D') answer += "-..";
            else if (ch == 'E') answer += ".";
            else if (ch == 'F') answer += "..-.";
            else if (ch == 'G') answer += "--.";
            else if (ch == 'H') answer += "....";
            else if (ch == 'I') answer += "..";
            else if (ch == 'J') answer += ".---";
            else if (ch == 'K') answer += "-.-";
            else if (ch == 'L') answer += ".-..";
            else if (ch == 'M') answer += "--";
            else if (ch == 'N') answer += "-.";
            else if (ch == 'O') answer += "---";
            else if (ch == 'P') answer += ".--.";
            else if (ch == 'Q') answer += "--.-";
            else if (ch == 'R') answer += ".-.";
            else if (ch == 'S') answer += "...";
            else if (ch == 'T') answer += "-";
            else if (ch == 'U') answer += "..-";
            else if (ch == 'V') answer += "...-";
            else if (ch == 'W') answer += ".--";
            else if (ch == 'X') answer += "-..-";
            else if (ch == 'Y') answer += "-..-";
            else if (ch == 'Z') answer += "--..";
            else if (ch == '1') answer += ".----";
            else if (ch == '2') answer += "..---";
            else if (ch == '3') answer += "...--";
            else if (ch == '4') answer += "....-";
            else if (ch == '5') answer += ".....";
            else if (ch == '6') answer += "-.....";
            else if (ch == '7') answer += "--...";
            else if (ch == '8') answer += "---..";
            else if (ch == '9') answer += "----.";
            else if (ch == '0') answer += "-----";
            else if (ch == ' ') answer += " ";

            else System.out.println(" invalid input ");
            answer += " ";

        }
        return answer;

    }
}
