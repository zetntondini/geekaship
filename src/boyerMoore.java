/**
 * Created by intern on 2015/10/09.
 */
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

public class boyerMoore {
    public static List<Integer> match(String pattern, String text) {
        List<Integer> matches = new ArrayList<Integer>();

        int txt = text.length();
        int patten = pattern.length();

        Map<Character, Integer> rightMostIndexes = preprocessForBadCharacterShift(pattern);
        int alignedAt = 0;

        while (alignedAt + (patten - 1) < txt) {
            for (int indexInPattern = patten - 1; indexInPattern >= 0; indexInPattern--) {
                int indexInText = alignedAt + indexInPattern;
                char txtChar = text.charAt(indexInText);
                char pattenChar = pattern.charAt(indexInPattern);

                if (indexInText >= txt)
                    break;
                if (txtChar != pattenChar) {
                    Integer r = rightMostIndexes.get(txtChar);
                    if (r == null) {
                        alignedAt = indexInText + 1;
                    } else {
                        int shift = indexInText - (alignedAt + r);
                        alignedAt += shift > 0 ? shift : 1;
                    }
                    break;
                } else if (indexInPattern == 0) {
                    matches.add(alignedAt);
                    alignedAt++;
                }
            }
        }
        return matches;
    }

    private static Map<Character, Integer> preprocessForBadCharacterShift(
            String pattern) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        for (int i = pattern.length() - 1; i >= 0; i--) {
            char c = pattern.charAt(i);
            if (!map.containsKey(c)) map.put(c, i);
        }
        return map;
    }

    public static void main(String[] args) {
        List<Integer> matches = match("av", "java");
        for (Integer integer : matches) System.out.println("Match: " + integer);
        System.out.println((matches.equals(Arrays.asList(1, 3)) ? "not" : "Fail"));
    }
}

