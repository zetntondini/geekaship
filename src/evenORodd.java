/**
 * Created by intern on 2015/10/09.
 */
import java.util.Scanner;
public class evenORodd {

    public static void main(String args[])
    {
        int number;
        System.out.println("Enter a number");
        Scanner in = new Scanner(System.in);
         number= in.nextInt();

        if ( number % 2 == 0 )
            System.out.println("Even number");
        else
            System.out.println("Odd number");
    }
}
